package es.udc.fi.lbd.monuzz.id.xogos.daos;

import java.util.List;

import es.udc.fi.lbd.monuzz.id.xogos.model.Programador;
import es.udc.fi.lbd.monuzz.id.xogos.model.Subscritor;
import es.udc.fi.lbd.monuzz.id.xogos.model.Xogo;


public class XogoDAOHibImpl implements XogoDAO {

	@Override
	public Long create(Xogo meuXogo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(Xogo meuXogo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void remove(Xogo meuXogo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Xogo findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Xogo findByCodigo(String codigo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Xogo> findAllXogosProgramador(Programador meuProgramador) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Xogo> findAllXogosSubscritor(Subscritor meuSubscritor) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Subscritor> findAllSubscritoresXogo(Xogo meuXogo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Xogo> findAllXogosSenSubscritores() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Xogo> findAllXogosSenSubscritoresProgramador(Programador meuProgramador) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long findNumXogosProProgramador(Programador meuProgramador) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Xogo> findAllXogosProProgramador(Programador meuProgramador) {
		// TODO Auto-generated method stub
		return null;
	}



}
