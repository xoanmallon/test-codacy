package es.udc.fi.lbd.monuzz.id.xogos.model;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;


@SuppressWarnings("unused")


public class Xogo {
	
	private Long idXogo;
	String codigo;
	String categoria;
	private Timestamp dataAlta;
	private Timestamp dataPro;
	private Programador programador;
	 

	// Chave natural: codigo
	// Atributos obrigatorios: codigo, categoría, dataAlta, programador
	// Atributos únicos: codigo
	
	// O conxunto de subscritores asociados ao xogo non ten orde preestablecida
	
	private Xogo() {}

	public Xogo(String codigo, String categoria, Timestamp dataAlta) {
		this.codigo = codigo;
		this.categoria = categoria;
		this.dataAlta = dataAlta;
		this.dataPro = null;
	}

	public Long getIdXogo() {
		return idXogo;
	}
	public String getCodigo() {
		return codigo;
	}
	public String getCategoria() {
		return categoria;
	}
	public Timestamp getDataAlta() {
		return dataAlta;
	}	
	public Timestamp getDataPro() {
		return dataPro;
	}
	public Programador getProgramador() {
		return programador;
	}

	public void setIdXogo(Long idOrde) {
		this.idXogo = idOrde;
	}
	public void setCodigo(String codigo) {
		this.codigo=codigo;
	}
	public void setCategoria(String categoria) {
		this.categoria=categoria;
	}
	public void setDataAlta(Timestamp dataAlta) {
		this.dataAlta = dataAlta;
	}
	public void setDataPro(Timestamp dataPro) {
		this.dataPro = dataPro;
	}
	public void setProgramador(Programador programador) {
		this.programador = programador;
	}
	


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Xogo other = (Xogo) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Incidencia [idIncidencia=" + idXogo + ", codigo=" + codigo + ", categoria=" + categoria + ", dataAlta=" + dataAlta
				+ ", dataResolucion=" + dataPro + ", programador=" + programador + "]";
	}


	
	
}
