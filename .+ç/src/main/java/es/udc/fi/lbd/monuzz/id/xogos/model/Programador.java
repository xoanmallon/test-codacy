package es.udc.fi.lbd.monuzz.id.xogos.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


@SuppressWarnings("unused")


public class Programador extends Usuario {

	private String alias;
	private List <Xogo> xogos = new ArrayList<Xogo>();

	// Atributos obligatorios: alias
	// Atributos únicos: alias
	// Ademais, debe ser imposible que un programador estea asociado dúas veces ao mesmo xogo na BD
	// Lista de xogos: o mais recente de primeiro (orde por data de alta, descendente)

	private Programador() {
	}

	public Programador(String login, String password, String nome, Timestamp dataAlta, 
			String alias) 
	{
		super(login, password, nome, dataAlta);
		this.alias = alias;
	}

	public String getAlias() {
		return this.alias;
	}
	
	public List<Xogo> getXogos() {
		return this.xogos;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public void setXogos(List<Xogo> xogos) {
		this.xogos = xogos;
	}

	// Método de conveniencia para dar de alta un novo xogo do programador
	// Nota: en memoria, NON EN DISCO
	// Nota: inserimos polo PRINCIPIO (os mais recentes primeiro)
	
	public void altaXogo(Xogo meuXogo) {
		if (this.xogos.contains(meuXogo)) 
			throw new RuntimeException("O xogo xa estaba vinculado ao programador: " + this.toString());
		if (meuXogo.getProgramador()!=null)
			throw new RuntimeException("O xogo xa está vinculado a outro programador: " + meuXogo.getProgramador().toString());			
		meuXogo.setProgramador(this);
		this.xogos.add(0, meuXogo);

	}
	
	// Método de conveniencia para dar de baixa unha incidencia do cliente
	// Nota: en memoria, NON EN DISCO

	public Boolean baixaXogo(Xogo meuXogo) {
		if (!meuXogo.getProgramador().equals(this))
			throw new RuntimeException("O xogo non está viculado ao programador: " + this.toString());			
		if (! this.xogos.contains(meuXogo)) 
			throw new RuntimeException("O xogo non está viculado ao programador: " + this.toString());
		meuXogo.setProgramador(null);
		return (this.xogos.remove(meuXogo));
	}
	
	@Override
	public String toString() {
		return "Cliente [idUsuario=" + idUsuario + ", login=" + login + ", password=" + password
				+ ", nome=" + nome + ", dataAlta=" + dataAlta + ", alias=" + alias + "]";
	}
	
}
