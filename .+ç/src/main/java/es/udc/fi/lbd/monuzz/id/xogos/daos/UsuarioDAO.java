package es.udc.fi.lbd.monuzz.id.xogos.daos;

import java.util.List;

import es.udc.fi.lbd.monuzz.id.xogos.model.Programador;
import es.udc.fi.lbd.monuzz.id.xogos.model.Subscritor;
import es.udc.fi.lbd.monuzz.id.xogos.model.Usuario;

public interface UsuarioDAO {
	
	public Long create(Usuario meuUsuario);
		//Rexistra un novo usuario na BD
		//Excepcion se o usuario xa é persistente
	public void update (Usuario meuUsuario);
		//Modifica os datos dun usuario na BD
		//Excepcion se o usuario non é persistente ainda	
	public void remove (Usuario meuUsuario);
		//Elimina os datos dun usuario da BD
		//Excepcion se o usuario non é persistente ainda

	// -----------------------------------------------------------------------------

	public Usuario findById(Long id);
		//Recupera os datos dun usuario desde a BD usando o seu id persistente
	public Usuario findByLogin (String login);	
		//Recupera os datos dun usuario desde a BD usando o seu login 

	// -----------------------------------------------------------------------------

	public List<Usuario> findAll();
		//Recupera TODOS os programadores e subscritores rexistrados na BD, ordenados por data de alta (o mais recente ao FINAL) e login (A-Z)
	public List<Programador> findAllProgramadores();
		//Recupera TODOS os programadores (por orde de data de alta, o mais recente ao PRINCIPIO) e login (A-Z)
	public List<Subscritor> findAllSubscritores();
		//Recupera TODOS os subscritores (por orde de data de alta, o mais recente ao PRINCIPIO) e login (A-Z)

}
