package es.udc.fi.lbd.monuzz.id.xogos;

import static org.junit.Assert.*;

import java.sql.Timestamp;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.udc.fi.lbd.monuzz.id.xogos.model.*;
import es.udc.fi.lbd.monuzz.id.xogos.services.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/spring-config.xml", "/test-spring-config.xml"})
@ActiveProfiles("test")
public class TestUsuarios {
	
	private Logger log = Logger.getLogger("xogos");

	@Autowired
	private TestUtils testUtils;

	@Autowired
	private UsuarioService usuarioService;
	
	@Before
	public void setUp() throws Exception {
		//Creamos usuarios de proba iniciais
		log.info ("Creando usuarios iniciais para caso de proba: " + this.getClass().getName());
		testUtils.creaSetUsuariosProba();
		log.info ("Datos de usuarios inicializados con éxito");
	}

	@After
	public void tearDown() throws Exception {
		log.info ("Eliminando usuarios iniciais para caso de proba: " + this.getClass().getName());
		testUtils.eliminaSetUsuariosProba();  
		log.info ("Datos de usuarios eliminados con éxito");
	}
	
	@Test
	public void testCompleto() {
		a_Test_Alta();
		b_Test_Actualizacion();
		c_Test_Borrado();
	}
	
	private void a_Test_Alta() {
		
				
		Subscritor subsX, subsY;
		Programador progX, progY;
		List<Subscritor> listaE;
		List<Programador> listaC;
		List<Usuario> listaU;
		Boolean error;

		// Alta dun novo subscritor (que se suma aos iniciais) e probamos recuperación -------------------------------------------------------------------------------------

		subsX = new Subscritor ("SUBSPROBA", "SubsProba", "SubscritorProba", Timestamp.valueOf("2017-01-01 10:00:00.0"), "NOVATO",  new Float(25));
		subsX.addEmail("subsx@empresa.com");

		usuarioService.altaNovoUsuarioBD(subsX);
		
		assertEquals(subsX, usuarioService.recuperarUsuarioBDPorId(subsX.getIdUsuario()));
		assertEquals(subsX, usuarioService.recuperarUsuarioBDPorLogin(subsX.getLogin()));

		listaE = usuarioService.recuperarTodosSubscritoresBD();
		assertEquals (listaE.size(), 3);
		assertEquals (listaE.get(0),subsX);
		assertEquals (listaE.get(1),testUtils.subsB);
		assertEquals (listaE.get(2),testUtils.subsA);
		
		// Alta dun novo programador, e probamos recuperacion -------------------------------------------------------------------------------------

		progX = new Programador ("PROGPROBA", "ProgProba", "ProgramadorProba", Timestamp.valueOf("2017-01-01 10:05:00.0"), "Naruto");
		progX.addEmail("progx@mailmail.com");

		usuarioService.altaNovoUsuarioBD(progX);
		
		assertEquals(progX, usuarioService.recuperarUsuarioBDPorId(progX.getIdUsuario()));
		assertEquals(progX, usuarioService.recuperarUsuarioBDPorLogin(progX.getLogin()));

		listaC = usuarioService.recuperarTodosProgramadoresBD();
		assertEquals (listaC.size(), 4);
		assertEquals (listaC.get(0),progX);
		assertEquals (listaC.get(1),testUtils.progC);
		assertEquals (listaC.get(2),testUtils.progB);
		assertEquals (listaC.get(3),testUtils.progA);

		// Recuperación de TODOS os usuarios  -------------------------------------------------------------------------------------

		listaU = usuarioService.recuperarTodosUsuariosBD();
		assertEquals (listaU.size(), 7);
		assertEquals (listaU.get(0),testUtils.subsA);
		assertEquals (listaU.get(1),testUtils.subsB);
		assertEquals (listaU.get(2),testUtils.progA);
		assertEquals (listaU.get(3),testUtils.progB);
		assertEquals (listaU.get(4),testUtils.progC);
		assertEquals (listaU.get(5),subsX);
		assertEquals (listaU.get(6),progX);

		
		// Deteccion duplicado -------------------------------------------------------------------------------------
		
		//login
		
		try {
			error=false;
			subsY  = new Subscritor ("SUBSPROBA", "SubsProba", "SubscritorProba", Timestamp.valueOf("2017-01-01 10:00:00.0"), "NOVATO",  new Float(15));
			usuarioService.altaNovoUsuarioBD(subsY);
		} catch (DataIntegrityViolationException e){
			error=true;
		}
		assertTrue(error);
		
		try {
			error=false;
			progY = new Programador ("PROGPROBA", "ProgProba", "ProgramadorProba", Timestamp.valueOf("2017-01-01 10:05:00.0"), "Naruto");
			usuarioService.altaNovoUsuarioBD(progY);
		} catch (DataIntegrityViolationException e){
			error=true;
		}
		assertTrue(error);

		// alias 
		
		try {
			error=false;
			progY = new Programador ("PROGPROBA2", "ProgProba2", "ProgramadorProba2", Timestamp.valueOf("2017-01-01 10:05:00.0"), "Naruto");
			usuarioService.altaNovoUsuarioBD(progY);
		} catch (DataIntegrityViolationException e){
			error=true;
		}
		assertTrue(error);

		//Deteccion login nulo -------------------------------------------------------------------------------------
		
		try {
			error=false;
			subsY = new Subscritor (null, "SubsProba2", "SubscritorProba2", Timestamp.valueOf("2017-01-01 10:00:00.0"), "NOVATO",  new Float(15));
			usuarioService.altaNovoUsuarioBD(subsY);
		} catch (DataIntegrityViolationException e){
			error=true;
		}
		assertTrue(error);
		
		try {
			error=false;
			progY = new Programador (null, "ProgProba2", "ProgramadorProba2", Timestamp.valueOf("2017-01-01 10:05:00.0"), "Naruto");
			usuarioService.altaNovoUsuarioBD(progY);
		} catch (DataIntegrityViolationException e){
			error=true;
		}
		assertTrue(error);


		//Deteccion password nulo -------------------------------------------------------------------------------------
		
		try {
			error=false;
			subsY = new Subscritor ("SUBSPROBA2", null, "SubscritorProba2", Timestamp.valueOf("2017-01-01 10:00:00.0"), "NOVATO",  new Float(15));
			usuarioService.altaNovoUsuarioBD(subsY);
		} catch (DataIntegrityViolationException e){
			error=true;
		}
		assertTrue(error);
		
		try {
			error=false;
			progY = new Programador ("PROGPROBA2", null, "ProgramadorProba2", Timestamp.valueOf("2017-01-01 10:05:00.0"), "Naruto");
			usuarioService.altaNovoUsuarioBD(progY);
		} catch (DataIntegrityViolationException e){
			error=true;
		}
		assertTrue(error);

		//Deteccion nome nulo -------------------------------------------------------------------------------------
		
		try {
			error=false;
			subsY = new Subscritor ("SUBSPROBA2", "subsproba2", null, Timestamp.valueOf("2017-01-01 10:00:00.0"), "NOVATO",  new Float(15));
			usuarioService.altaNovoUsuarioBD(subsY);
		} catch (DataIntegrityViolationException e){
			error=true;
		}
		assertTrue(error);
		
		try {
			error=false;
			progY = new Programador ("PROGPROBA2", "progproba2", null, Timestamp.valueOf("2017-01-01 10:05:00.0"), "Naruto");
			usuarioService.altaNovoUsuarioBD(progY);
		} catch (DataIntegrityViolationException e){
			error=true;
		}
		assertTrue(error);

		//Deteccion data alta nula -------------------------------------------------------------------------------------
		
		try {
			error=false;
			subsY = new Subscritor ("SUBSPROBA2", "subsproba2", "Subscritorproba2", null, "NOVATO",  new Float(15));
			usuarioService.altaNovoUsuarioBD(subsY);
		} catch (DataIntegrityViolationException e){
			error=true;
		}
		assertTrue(error);
		
		try {
			error=false;
			progY = new Programador ("PROGPROBA2", "progproba2", "Programadorproba2", null , "Naruto");
			usuarioService.altaNovoUsuarioBD(progY);
		} catch (DataIntegrityViolationException e){
			error=true;
		}
		assertTrue(error);

		//Deteccion categoria nula -------------------------------------------------------------------------------------

		try {
			error=false;
			subsY = new Subscritor ("SUBSPROBA2", "subsproba2", "Subscritorproba2", Timestamp.valueOf("2017-01-01 10:00:00.0"), null,  new Float(15));
			usuarioService.altaNovoUsuarioBD(subsY);
		} catch (DataIntegrityViolationException e){
			error=true;
		}
		assertTrue(error);
		
		
		// Comprobamos que as insercions erroneas non funcionaron ----------------------------------------------------------
		
		listaU = usuarioService.recuperarTodosUsuariosBD();
		assertEquals (listaU.size(), 7);
	}
	
	
	
	private void b_Test_Actualizacion() {
		
		Subscritor subsX, subsY;
		Programador progX, progY;
		
		// Recuperamos ao noso subscritor e cambiamos algun dos seus datos
		
		subsX = (Subscritor) usuarioService.recuperarUsuarioBDPorLogin("SUBSPROBA");
		progX = (Programador) usuarioService.recuperarUsuarioBDPorLogin("PROGPROBA");

		// Subscritor: mudar nome, e puntos ----------------------------------------------------------

		subsX.setNome("Nome novo de subscritor");
		subsX.setPuntos(new Float(30));
		usuarioService.modificacionUsuarioBD(subsX);
		subsY = (Subscritor) usuarioService.recuperarUsuarioBDPorId(subsX.getIdUsuario());
		assertEquals(subsX.getNome(), subsY.getNome());
		assertEquals(subsX.getPuntos(), subsY.getPuntos());
		
		// Programador: mudar nome, e alias ----------------------------------------------------------
		
		progX.setNome("Nome novo de programador");
		progX.setAlias("Novo alias");
		usuarioService.modificacionUsuarioBD(progX);
		progY = (Programador) usuarioService.recuperarUsuarioBDPorId(progX.getIdUsuario());
		assertEquals(progX.getNome(), progY.getNome());
		assertEquals(progX.getAlias(), progY.getAlias());
		
		// Engadir email ----------------------------------------------------------
		
		assertEquals(subsX.getEmails().size(), 1);
		assertEquals(progX.getEmails().size(), 1);
		
		subsX.addEmail("subsx@mailnovo.org");
		progX.addEmail("progx@mailnovo.org");

		usuarioService.modificacionUsuarioBD(subsX);
		usuarioService.modificacionUsuarioBD(progX);

		subsY = (Subscritor) usuarioService.recuperarUsuarioBDPorId(subsX.getIdUsuario());
		progY = (Programador) usuarioService.recuperarUsuarioBDPorId(progX.getIdUsuario());
		
		assertEquals(subsY.getEmails().size(), 2);
		assertTrue (subsY.getEmails().contains("subsx@mailnovo.org"));
		assertEquals(subsX.getEmails(), subsY.getEmails());
		
		assertEquals(progY.getEmails().size(), 2);
		assertTrue (progY.getEmails().contains("progx@mailnovo.org"));
		assertEquals(progX.getEmails(), progY.getEmails());
		
		// Eliminar email ----------------------------------------------------------
		
		subsX.removeEmail("subsx@mailnovo.org");
		progX.removeEmail("progx@mailnovo.org");
		
		usuarioService.modificacionUsuarioBD(subsX);
		usuarioService.modificacionUsuarioBD(progX);

		subsY = (Subscritor) usuarioService.recuperarUsuarioBDPorId(subsX.getIdUsuario());
		progY = (Programador) usuarioService.recuperarUsuarioBDPorId(progX.getIdUsuario());
		
		assertEquals(subsY.getEmails().size(), 1);
		assertFalse (subsY.getEmails().contains("subsx@mailnovo.org"));
		
		assertEquals(progY.getEmails().size(), 1);
		assertFalse (progY.getEmails().contains("progx@mailnovo.org"));
	}

	
	private void c_Test_Borrado() {
		
		Subscritor subsX;
		Programador progX;
		Boolean error;

		// Intento de borrado dun usuario que non existe

		subsX = new Subscritor ("SUBSPROBAINEXISTENTE", "SubsPoba", "SubscritorProba", Timestamp.valueOf("2017-01-01 10:00:00.0"), "NOVATO",  new Float(15));
		subsX.addEmail("subsx@empresa.com");
		
		progX = new Programador ("PROGPROBAINEXISTENTE", "ProgProba", "ProgramadorProba", Timestamp.valueOf("2017-01-01 10:05:00.0"), "Naruto");
		progX.addEmail("progp@mailmail.com");
		
		try {
			error=false;
			usuarioService.borradoUsuarioBD(subsX);
		} catch (RuntimeException e){
			error=true;
		}
		assertTrue(error);
		
		try {
			error=false;
			usuarioService.borradoUsuarioBD(progX);
		} catch (RuntimeException e){
			error=true;
		}
		assertTrue(error);

		// Eliminamos usuarios de proba
		
		subsX = (Subscritor) usuarioService.recuperarUsuarioBDPorLogin("SUBSPROBA");
		progX = (Programador) usuarioService.recuperarUsuarioBDPorLogin("PROGPROBA");

		usuarioService.borradoUsuarioBD(subsX);
		usuarioService.borradoUsuarioBD(progX);
		
		assertNull (usuarioService.recuperarUsuarioBDPorLogin("SUBSPROBA"));
		assertNull (usuarioService.recuperarUsuarioBDPorLogin("PROGPROBA"));
	}
	
}
