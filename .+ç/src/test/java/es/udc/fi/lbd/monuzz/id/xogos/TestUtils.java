package es.udc.fi.lbd.monuzz.id.xogos;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;

import es.udc.fi.lbd.monuzz.id.xogos.model.*;
import es.udc.fi.lbd.monuzz.id.xogos.services.*;

public class TestUtils {

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private XogoService xogoService;
	
	
	public final long timeout = 50;
	
	public Programador progA;
	public Programador progB;
	public Programador progC;

	public Subscritor subsA;
	public Subscritor subsB;

	public Xogo xogo_A1;
	public Xogo xogo_A2;
	public Xogo xogo_B1;
	
	

	public void creaSetUsuariosProba() {

		// Subscritores
		
		// subscritor(String login, String password, String nome, Timestamp dataAlta, String nivel, Float puntos)
		
		subsA = new Subscritor ("SUBS1", "Subs1", "Subscritor1", Timestamp.valueOf("2016-01-01 10:00:00.0"), "SUBSCRITOR OURO",  new Float(725));
		subsB = new Subscritor ("SUBS2", "Subs2", "Subscritor2", Timestamp.valueOf("2016-01-31 10:00:00.0"), "NOVATO",  new Float(15));
		
		subsA.addEmail("subsA@empresa.com");
		subsA.addEmail("subsA@email.com");
		subsB.addEmail("subsB@empresa.com");
		
		usuarioService.altaNovoUsuarioBD(subsA);
		usuarioService.altaNovoUsuarioBD(subsB);

		
		// Programadores

		// Programador(String login, String password, String nome, Timestamp dataAlta, String alias) 
		progA = new Programador ("PROGA", "ProgA", "ProgramadorA", Timestamp.valueOf("2016-02-01 10:00:00.0"), "Hacker1");
		progB = new Programador ("PROGB", "ProgB", "ProgramadorB", Timestamp.valueOf("2016-02-27 15:00:00.0"), "Bill puertas");
		progC = new Programador ("PROGC", "ProgC", "ProgramadorC", Timestamp.valueOf("2016-03-31 17:00:00.0"), "Neo");
		
		progA.addEmail("progA@micromail.com");
		progA.addEmail("progA@traballo.com");
		
		progB.addEmail("progB@macromail.com");
		progB.addEmail("progB@outrotraballo.com");

		progC.addEmail("progC@outrotraballomais.com");

		usuarioService.altaNovoUsuarioBD(progA);
		usuarioService.altaNovoUsuarioBD(progB);		
		usuarioService.altaNovoUsuarioBD(progC);		

	}
	
	public void eliminaSetUsuariosProba() {

		usuarioService.borradoUsuarioBD(progC);
		usuarioService.borradoUsuarioBD(progB);
		usuarioService.borradoUsuarioBD(progA);

		usuarioService.borradoUsuarioBD(subsB);
		usuarioService.borradoUsuarioBD(subsA);

	}

	public void creaSetXogosProba() {
		
		// Crea tres xogos vinculados aos programadores creados anteriormente
		
		xogo_A1 = new Xogo("XOGO_A1/2017", "FPS",  Timestamp.valueOf("2017-01-25 10:35:14.0"));
		xogo_A2 = new Xogo("XOGO_A2/2017", "RPG", Timestamp.valueOf("2017-01-28 22:29:00.0"));
		progA.altaXogo(xogo_A1);
		progA.altaXogo(xogo_A2);		
		xogo_B1 = new Xogo("INC_B1/2017", "Arcade", Timestamp.valueOf("2017-02-15 11:02:29.0"));
		progB.altaXogo(xogo_B1);		

		xogoService.altaNovoXogoBD(xogo_A1);
		xogoService.altaNovoXogoBD(xogo_A2);
		xogoService.altaNovoXogoBD(xogo_B1);
		
	}

	public void eliminaSetXogosProba() {

		xogoService.borradoXogoBD(xogo_A1);
		xogoService.borradoXogoBD(xogo_A2);
		xogoService.borradoXogoBD(xogo_B1);
	}
	
}
