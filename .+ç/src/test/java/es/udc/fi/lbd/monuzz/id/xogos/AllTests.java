package es.udc.fi.lbd.monuzz.id.xogos;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestUsuarios.class, TestXogos.class } )
public class AllTests {

}
