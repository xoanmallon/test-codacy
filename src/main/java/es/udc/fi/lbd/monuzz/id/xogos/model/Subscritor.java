package es.udc.fi.lbd.monuzz.id.xogos.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")


public class Subscritor extends Usuario {

	private String nivel;
	private Float puntos;
	protected List<Xogo> subscricions = new ArrayList<Xogo> (); 
	
	// Atributos obrigatorios: nivel
	// Atributos únicos: Debe ser imposible que un subscritor estea asociado dúas veces ao mesmo xogo na BD
	// Lista de xogos: o mais recente de primeiro (orde por data de alta, descendente)


	private  Subscritor() {
	}

	public Subscritor(String login, String password, String nome, Timestamp dataAlta, 
			String nivel, Float puntos) {
		super(login, password, nome, dataAlta);
		this.nivel = nivel;
		this.puntos = puntos;		

	}

	public String getNivel() {
		return nivel;
	}
	public Float getPuntos() {
		return this.puntos;
	}

	public List<Xogo> getSubscricions() {
		return this.subscricions;
	}	

	public void setNivel(String posto) {
		this.nivel = posto;
	}
	public void setPuntos(Float puntos) {
		this.puntos = puntos;
	}

	public void setSubscricions(List<Xogo> subscricions){
		this.subscricions = subscricions;
	}

	// Método de conveniencia para dar de alta unha nova subscricion
	// Nota: en memoria, NON EN DISCO
	
	public void altaXogo(Xogo meuXogo) {
		if (this.subscricions.contains(meuXogo)) 
			throw new RuntimeException("O xogo xa estaba vinculado ao usuario: " + this.toString());
		this.subscricions.add(0, meuXogo);

	}
	
	// Método de conveniencia para dar de baixa unha subscrición do usuario
	// Nota: en memoria, NON EN DISCO

	public Boolean baixaXogo(Xogo meuXogo) {
		if (! this.subscricions.contains(meuXogo)) 
			throw new RuntimeException("O xogo non está viculado ao usuario: " + this.toString());
		return (this.subscricions.remove(meuXogo));
	}
	
	@Override
	public String toString() {
		return "Consumidor [idUsuario=" + idUsuario + ", nome=" + nome + ", login=" + login + ", password=" + password + ",dataAlta=" + dataAlta +  
				", nivel=" + nivel + ", puntos=" + puntos + "]";
	}
			
	
	

	
}
