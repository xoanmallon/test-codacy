package es.udc.fi.lbd.monuzz.id.xogos.services;

import java.util.List;

import es.udc.fi.lbd.monuzz.id.xogos.model.Programador;
import es.udc.fi.lbd.monuzz.id.xogos.model.Subscritor;
import es.udc.fi.lbd.monuzz.id.xogos.model.Xogo;

public class XogoServiceImpl implements XogoService {

	@Override
	public void altaNovoXogoBD(Xogo meuXogo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void borradoXogoBD(Xogo meuXogo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void modificacionXogoBD(Xogo meuXogo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Xogo recuperarXogoBDPorId(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Xogo recuperarXogoBDPorCodigo(String codigo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Xogo> recuperarXogosProgramadorBD(Programador meuProgramador) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Xogo> recuperarXogosSubscritorBD(Subscritor meuSubscritor) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Subscritor> recuperarSubscritoresXogoBD(Xogo meuXogo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Xogo> recuperarXogosSenSubscritoresBD() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Xogo> recuperarXogosSenSubscritoresProgramadorBD(Programador meuProgramador) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long recuperarNumXogosProProgramadorBD(Programador meuProgramador) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Xogo> recuperarXogosProProgramadorBD(Programador meuProgramador) {
		// TODO Auto-generated method stub
		return null;
	}



}
