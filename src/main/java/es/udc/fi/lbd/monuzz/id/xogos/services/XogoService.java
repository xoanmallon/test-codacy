package es.udc.fi.lbd.monuzz.id.xogos.services;

import java.util.List;

import es.udc.fi.lbd.monuzz.id.xogos.model.Programador;
import es.udc.fi.lbd.monuzz.id.xogos.model.Subscritor;
import es.udc.fi.lbd.monuzz.id.xogos.model.Xogo;


public interface XogoService {
	
	// NOTA: Todos os métodos DEBEN rexistrar no log CALQUERA ERRO OU EXCEPCION QUE SE PRODUZA
	
	void altaNovoXogoBD (Xogo meuXogo);
		// Rexistra un novo xogo na BD. 
		// De se producir un problema, salta excepcion
	void borradoXogoBD (Xogo meuXogo);
		// Elimina os datos dun xogo na BD.
		// De se producir un problema, salta excepcion	
		// NOTA: non se pode borrar un xogo que ainda ten subscritores (saltará excepción)
	void modificacionXogoBD (Xogo meuXogo);
		// Actualiza os datos dun xogo  na BD. 
		// De se producir un problema, salta excepcion

	// -------------------------------------------------------------------------------

	Xogo recuperarXogoBDPorId(Long id);
		// Carga un xogo desde a BD usando o seu id persistente
	Xogo recuperarXogoBDPorCodigo(String codigo);
		// Carga un xogo desde a BD usando o seu codigo (único)

	// -------------------------------------------------------------------------------
	
	List<Xogo> recuperarXogosProgramadorBD(Programador meuProgramador);
		// Obten unha lista con todos os xogos rexistrados por un programador (na orde definida no DAO)
		// Carga a propiedade programador.xogos SE FOSE PRECISO
	List<Xogo> recuperarXogosSubscritorBD(Subscritor meuSubscritor);
		// Obten unha lista con todos os xogos aos que está subscrito un usuario (na orde definida no DAO)
		// Carga a propiedade subscritor.subscricions SE FOSE PRECISO
	List<Subscritor> recuperarSubscritoresXogoBD(Xogo meuXogo);
		// Obten todos os usuarios subscritos a un xogo (na orde definida no DAO)

	// -------------------------------------------------------------------------------
	
	List<Xogo> recuperarXogosSenSubscritoresBD();
		// Obten unha lista con todos os xogos (de calquera programador), que ainda non teñen subscritores (na orde definida no DAO)
	List<Xogo> recuperarXogosSenSubscritoresProgramadorBD(Programador meuProgramador);
		// Obten unha lista con todos os xogos rexistrados por un programador determinado, e que ainda non teñen subscritores (na orde definida no DAO)

	// -------------------------------------------------------------------------------

	Long recuperarNumXogosProProgramadorBD(Programador meuProgramador);
		// Obten o número de xogos creados por un programador determinado, e que pasaron a categoria "Pro" (condicion: "dataPro" <> nulo)
	List<Xogo> recuperarXogosProProgramadorBD(Programador meuProgramador);
		// Obten unha lista con todos as xogos creados por un programador, e que pasaron a categoria "pro" (na orde definida no DAO)
	
}
