package es.udc.fi.lbd.monuzz.id.xogos.daos;

import java.util.List;

import es.udc.fi.lbd.monuzz.id.xogos.model.Programador;
import es.udc.fi.lbd.monuzz.id.xogos.model.Subscritor;
import es.udc.fi.lbd.monuzz.id.xogos.model.Xogo;

public interface XogoDAO {
	public Long create(Xogo meuXogo);
		//Rexistra un novo xogo na BD
		//Excepcion se o xogo xa é persistente
	public void update (Xogo meuXogo);
		//Modifica os datos dun xogo na BD
		//Excepcion se o xogo non é persistente ainda	
	public void remove (Xogo meuXogo);
		//Elimina os datos dun xogo da BD
		//Excepcion se o xogo non é persistente ainda

	// -----------------------------------------------------------------------------

	public Xogo findById(Long id);
		//Recupera os datos dun xogo desde a BD usando o seu id persistente
	public Xogo findByCodigo (String codigo);	
		//Recupera os datos dun xogo desde a BD usando o seu codigo 

	// -----------------------------------------------------------------------------

	public List<Xogo> findAllXogosProgramador(Programador meuProgramador);
		//Recupera TODOS os xogos dun programador (por orde de data de alta, o mais recente ao PRINCIPIO)	
	public List<Xogo> findAllXogosSubscritor(Subscritor meuSubscritor);
		//Recupera TODOS os xogos dun subscritor (por orde de data de alta, o mais recente ao PRINCIPIO) 	
	public List<Subscritor> findAllSubscritoresXogo(Xogo meuXogo);
		//Recupera TODOS os subscritores dun xogo, por orde de login (A-Z)  	

	// -----------------------------------------------------------------------------

	public List<Xogo> findAllXogosSenSubscritores();
		//Recupera TODOS os xogos que non teñan subscritores (por orde de data de alta, o mais recente ao FINAL) 	
	public List<Xogo> findAllXogosSenSubscritoresProgramador(Programador meuProgramador);
		//Recupera TODOS os xogos dun programador, que non teñan subscritores (por orde de data de alta, o mais recente ao FINAL) 

	// -----------------------------------------------------------------------------

	public Long findNumXogosProProgramador(Programador meuProgramador);
		//Recupera o número de xogos con categoría Profesional dun programador (condicion: "dataPro" non nula)  	
	public List<Xogo> findAllXogosProProgramador(Programador meuProgramador);
		//Recupera TODOS os xogos con categoría Profesional dun programador (condicion: "dataPro" non nula), por orde de data de alta, o mais recente ao FINAL. 	

}
