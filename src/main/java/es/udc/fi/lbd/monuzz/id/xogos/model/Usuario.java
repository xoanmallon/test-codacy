package es.udc.fi.lbd.monuzz.id.xogos.model;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

@SuppressWarnings("unused")


public class Usuario {
	
	protected Long idUsuario;	
	protected String login;
	protected String password;
	protected String nome;
	protected Timestamp dataAlta;
	protected Set<String> emails = new HashSet<String>();
	
	// Chave natural: login
	// Atributos obrigatorios: login, password, nome, dataAlta
	// Atributos únicos: login
	// Ademais, debe ser imposible que un  usuario teña o mesmo email rexistrado dúas veces na BD
	
	protected Usuario() {	
	};
	
	protected Usuario(String login, String password, String nome, Timestamp dataAlta) {
		this.login = login;
		this.password = password;
		this.nome = nome;
		this.dataAlta = dataAlta;
	}


	public Long getIdUsuario() {
		return this.idUsuario;
	}
	

	public String getLogin() {
		return this.login;
	}
	public String getPassword() {
		return this.password;
	}
	public String getNome() {
		return this.nome;
	}    
	public Timestamp getDataAlta() {
		return this.dataAlta;
	}
	public Set<String> getEmails() {
		return this.emails;
	}
		
	private void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public void setDataAlta(Timestamp dataAlta) {
		this.dataAlta = dataAlta;
	}
	public void setEmails(Set<String> emails) {
		this.emails = emails;
	}
	public void addEmail (String email) {
		if ( this.emails.contains(email))
			throw new RuntimeException ("Erro: email duplicado");	
		this.emails.add(email);
	}

	public void removeEmail (String email){
		if ( ! this.emails.contains(email))
			throw new RuntimeException ("Erro: email non asociado ao usuario");	
		this.emails.remove(email);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		return true;
	}

	
	@Override
	public String toString() {
		return "Usuario [idUsuario=" + idUsuario + ", login=" + login + ", password=" + password + ", nome=" + nome
				+ ", dataAlta=" + dataAlta + "]";
	}

	public String emailsToString() {
		String texto = "Emails: [";
		for (String email : this.emails)
		{
		    texto+="(" + email + ") ";
		}
		texto += "]";
		return (texto);
	}


	
}
