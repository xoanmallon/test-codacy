package es.udc.fi.lbd.monuzz.id.xogos.services;
import java.util.List;

import es.udc.fi.lbd.monuzz.id.xogos.model.Programador;
import es.udc.fi.lbd.monuzz.id.xogos.model.Subscritor;
import es.udc.fi.lbd.monuzz.id.xogos.model.Usuario;

public interface UsuarioService {
		
	// NOTA: Todos os métodos DEBEN rexistrar no log CALQUERA ERRO OU EXCEPCION QUE SE PRODUZA
	
	void altaNovoUsuarioBD (Usuario meuUsuario);
		// Rexistra unha nova persoa na BD. 
		// Se se produce un problema, salta excepcion
	void borradoUsuarioBD (Usuario meuUsuario);
		// Elimina unha persoa da BD. 		
		// Se se produce un problema, salta excepcion
		// Nota: Programador asociado a xogos: os xogos TAMÉN se borran
		// Nota: Non se pode borrar un programador que ten xogos con subscritores (saltará excepcion)
	void modificacionUsuarioBD (Usuario meuUsuario);
		// Actualiza os datos dunha persoa rexistrada na BD.
		// Se se produce un problema, salta excepcion

	// -------------------------------------------------------------------------------

	Usuario recuperarUsuarioBDPorId(Long id);
		// Carga a unha persoa desde a BD usando o seu id persistente
	Usuario recuperarUsuarioBDPorLogin(String login);
		// Carga a unha persoa desde a BD usando o seu login (único)

	// -------------------------------------------------------------------------------

	List<Usuario> recuperarTodosUsuariosBD();
		//Recupera a todas as persoas rexistradas na BD, tanto programadores como subscritores (na orde definida no DAO)
	List<Programador> recuperarTodosProgramadoresBD();
		//Recupera a todos os programadores rexistrados na BD (na orde definida no DAO)
	List<Subscritor> recuperarTodosSubscritoresBD();
		//Recupera a todos os subscritores rexistrados na BD (na orde definida no DAO)
	
	
}
