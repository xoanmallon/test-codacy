package es.udc.fi.lbd.monuzz.id.xogos;

import static org.junit.Assert.*;

import java.sql.Timestamp;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import es.udc.fi.lbd.monuzz.id.xogos.model.*;
import es.udc.fi.lbd.monuzz.id.xogos.services.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/spring-config.xml", "/test-spring-config.xml"})
@ActiveProfiles("test")
public class TestXogos {
	
	private Logger log = Logger.getLogger("xogos");

	@Autowired
	private TestUtils testUtils;

	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private XogoService xogoService;

	@Before
	public void setUp() throws Exception { 
		log.info ("Inicializando usuarios iniciais para caso de proba: " + this.getClass().getName());
		testUtils.creaSetUsuariosProba();
		log.info ("Inicializando incidencias iniciais para caso de proba: " + this.getClass().getName());
		testUtils.creaSetXogosProba();
		log.info ("Datos inicializados con éxito");
	}

	@After
	public void tearDown() throws Exception {
		/* Ao eliminar un programador deben desaparecer os seus xogos      */
		/* Descomentar só se non se consegue facer funcionar tal e como está */
		/* Descomentar reducirá a calificación da práctica                   */
		/*
		log.info ("Eliminando xogos iniciais para caso de proba: " + this.getClass().getName());
		testUtils.eliminaSetXogosProba();  
		*/
		log.info ("Eliminando usuarios iniciais para caso de proba: " + this.getClass().getName());
		testUtils.eliminaSetUsuariosProba(); 

		log.info ("Datos eliminados con éxito");
	}
	
	@Test
	public void testCompleto() {
		a_Test_Alta();
		b_Test_Actualizacion();
		c_Test_Borrado();
	}
	
	private void a_Test_Alta() {
		
		Programador progX, progY;
		Xogo xogo_X1, xogo_X2, xogo_X3;
		List<Xogo> listaXogos;
		List<Subscritor> listaSubs;
		Boolean error;

		// Inicializacion
		// Recuperación dos xogos dun programador
			
		progX = (Programador) usuarioService.recuperarUsuarioBDPorLogin(testUtils.progA.getLogin());
		listaXogos = xogoService.recuperarXogosProgramadorBD(progX);
		assertEquals (2, listaXogos.size());
		assertEquals (2, progX.getXogos().size());

				

		// Alta de novos xogos a sumar aos anteriores
		
		xogo_X1 = new Xogo("XOGO_X1/2017", "RPG", Timestamp.valueOf("2017-04-25 10:35:14.0"));
		xogo_X2 = new Xogo("XOGO_X2/2017", "Shooter", Timestamp.valueOf("2017-04-28 10:35:14.0"));
		progX.altaXogo(xogo_X1);
		progX.altaXogo(xogo_X2);
		
		xogoService.altaNovoXogoBD(xogo_X1);
		xogoService.altaNovoXogoBD(xogo_X2);		
		
				// Probamos recuperacion por id e codigo
		
				xogo_X3 = xogoService.recuperarXogoBDPorId(xogo_X1.getIdXogo());
				assertEquals (xogo_X1, xogo_X3);
		
				xogo_X3 = xogoService.recuperarXogoBDPorCodigo(xogo_X1.getCodigo());
				assertEquals (xogo_X1, xogo_X3);

				// Comprobamos recuperación dos xogos dun programador: o mais recente primeiro
		
				progY = (Programador) usuarioService.recuperarUsuarioBDPorLogin(progX.getLogin());
				listaXogos = xogoService.recuperarXogosProgramadorBD(progY);
				
				assertEquals (4, progY.getXogos().size());
				assertEquals (4, listaXogos.size());		
				
				assertEquals (xogo_X2, listaXogos.get(0));
				assertEquals (xogo_X1, listaXogos.get(1));
				assertEquals (testUtils.xogo_A2, listaXogos.get(2));
				assertEquals (testUtils.xogo_A1, listaXogos.get(3));

				// Comprobación de recuperación de xogos e subscritores 
				
				listaXogos = xogoService.recuperarXogosSubscritorBD(testUtils.subsA); 
				assertTrue (testUtils.subsA.getSubscricions().isEmpty());
				assertTrue (listaXogos.isEmpty());
				listaSubs = xogoService.recuperarSubscritoresXogoBD(xogo_X1);
				assertTrue (listaSubs.isEmpty());
				listaSubs = xogoService.recuperarSubscritoresXogoBD(xogo_X2);
				assertTrue (listaSubs.isEmpty());
				
				// Comprobación de recuperación de xogos sen subscritores (o mais recente ao FINAL)
				
				listaXogos = xogoService.recuperarXogosSenSubscritoresBD();
				assertEquals(5, listaXogos.size());
				assertEquals(testUtils.xogo_A1, listaXogos.get(0));
				assertEquals(testUtils.xogo_A2, listaXogos.get(1));
				assertEquals(testUtils.xogo_B1, listaXogos.get(2));
				assertEquals(xogo_X1, listaXogos.get(3));
				assertEquals(xogo_X2, listaXogos.get(4));
				
				// Comprobación de recuperación de xogos sen subscritores dun programador (a mais recente ao FINAL)
		
				listaXogos = xogoService.recuperarXogosSenSubscritoresProgramadorBD(progY);
				assertEquals(4, listaXogos.size());
				assertEquals(testUtils.xogo_A1, listaXogos.get(0));
				assertEquals(testUtils.xogo_A2, listaXogos.get(1));
				assertEquals(xogo_X1, listaXogos.get(2));
				assertEquals(xogo_X2, listaXogos.get(3));
		
				// Comprobación de recuperación de xogos "Pro" dun programador
				
				assertEquals (new Long(0), xogoService.recuperarNumXogosProProgramadorBD(testUtils.progA)); 
			
				listaXogos = xogoService.recuperarXogosProProgramadorBD(testUtils.progA); 
				assertTrue (listaXogos.isEmpty());
		
		// Comprobación final: altas problemáticas
		
		// Deteccion de duplicados
		
		// a) Xogo xa existente
		try {
			error = false;
			xogoService.altaNovoXogoBD(xogo_X1);
		}
		catch (RuntimeException e){
			error=true;
		}
		assertTrue(error);

		// b) Xogo con datos duplicados
		
		progY = (Programador) usuarioService.recuperarUsuarioBDPorLogin(testUtils.progC.getLogin());
		xogoService.recuperarXogosProgramadorBD(progY);
		
		xogo_X3 = new Xogo(xogo_X1.getCodigo(), xogo_X1.getCategoria(), xogo_X1.getDataAlta());
		progY.altaXogo(xogo_X3);
		error = false;
		try {
			xogoService.altaNovoXogoBD(xogo_X3);
		}
		catch (DataAccessException e){
			progY.getXogos().clear();
			error=true;
		}
		assertTrue(error);

		// Detección de valores nulos
		
		// a) Codigo nulo
		
		xogo_X3 = new Xogo(null, "RPG", Timestamp.valueOf("2017-06-28 10:35:14.0"));
		progY.altaXogo(xogo_X3);
		error = false;

		try {
			xogoService.altaNovoXogoBD(xogo_X3);
		}
		catch (DataIntegrityViolationException e){
			progY.getXogos().clear();
			error=true;
		}
		assertTrue(error);

		// b) Categoría nula
		
		xogo_X3 = new Xogo("XOGO_XX", null, Timestamp.valueOf("2017-06-28 10:35:14.0"));
		progY.altaXogo(xogo_X3);
		error = false;

		try {
			xogoService.altaNovoXogoBD(xogo_X3);
		}
		catch (DataIntegrityViolationException e){
			progY.getXogos().clear();
			error=true;
		}
		assertTrue(error);
		
		// c) Data alta nula
		
		xogo_X3 = new Xogo("XOGO_XX", "RPG", null);
		progY.altaXogo(xogo_X3);
		error = false;

		try {
			xogoService.altaNovoXogoBD(xogo_X3);
		}
		catch (DataIntegrityViolationException e){
			progY.getXogos().clear();
			error=true;
		}
		assertTrue(error);

		
		// d) Programador nulo
		
		xogo_X3 = new Xogo("XOGO_XX", "RPG", Timestamp.valueOf("2017-06-28 10:35:14.0"));
		error = false;

		try {
			xogoService.altaNovoXogoBD(xogo_X3);
		}
		catch (DataIntegrityViolationException e){
			progY.getXogos().clear();
			error=true;
		}
		assertTrue(error);
		
	}
	
	private void b_Test_Actualizacion() {
		
		Programador progX, progY;
		Xogo xogo_X1, xogo_X2;
		List<Xogo> listaXogos;
		List<Subscritor> listaSubs;
		Boolean error;
		
		// Inicialización
		
		progX = (Programador) usuarioService.recuperarUsuarioBDPorLogin(testUtils.progA.getLogin());
		xogoService.recuperarXogosProgramadorBD(progX);
		xogo_X2 = progX.getXogos().get(0);
		xogo_X1 = progX.getXogos().get(1);
				
		progY = (Programador) usuarioService.recuperarUsuarioBDPorLogin(testUtils.progB.getLogin());
		xogoService.recuperarXogosProgramadorBD(progY);

		
		// Asignamos categoría "Pro" e comprobamos cambios na BD
		
		assertEquals (new Long(0), xogoService.recuperarNumXogosProProgramadorBD(testUtils.progA)); 
		
		xogo_X1.setDataPro(Timestamp.valueOf("2017-09-28 10:35:14.0"));
		xogoService.modificacionXogoBD(xogo_X1);
		xogo_X2.setDataPro(Timestamp.valueOf("2017-09-29 10:35:14.0"));
		xogoService.modificacionXogoBD(xogo_X2);
		
		assertEquals (new Long(2), xogoService.recuperarNumXogosProProgramadorBD(testUtils.progA)); 

		listaXogos = xogoService.recuperarXogosProProgramadorBD(testUtils.progA); 
		assertEquals (2, listaXogos.size());
		assertEquals (xogo_X1, listaXogos.get(0));
		assertEquals (xogo_X2, listaXogos.get(1));

		// Asignamos subscritor a xogo comprobamos cambios na BD
		// Cambio: subscribimos subsA a X2

		testUtils.subsA.altaXogo(xogo_X2);
		usuarioService.modificacionUsuarioBD(testUtils.subsA);
		
				// Comprobación de recuperación de xogos e subscritores 
				
				listaXogos = xogoService.recuperarXogosSubscritorBD(testUtils.subsA); 
				assertEquals (1, listaXogos.size());
				assertEquals (xogo_X2, listaXogos.get(0));

				listaSubs = xogoService.recuperarSubscritoresXogoBD(xogo_X2);
				assertEquals (1, listaSubs.size());
				assertTrue (listaSubs.contains(testUtils.subsA));

				// Comprobación de recuperación de xogos sen subscritores (o mais recente ao FINAL)
				// Xa non debe aparecer X2
		
				listaXogos = xogoService.recuperarXogosSenSubscritoresBD();
				assertEquals(4, listaXogos.size());
				assertFalse (listaXogos.contains(xogo_X2));
				
				// Comprobación de recuperación de xogos sen subscritores do programador (o mais recente ao FINAL)
				// Xa non debe aparecer X2
		
				listaXogos = xogoService.recuperarXogosSenSubscritoresProgramadorBD(progX);
				assertEquals(3, listaXogos.size());
				assertFalse (listaXogos.contains(xogo_X2));
				
		// Asignamos novos subscritores aos xogos. 
		// X1 asignado a subsA e subsB
				
		testUtils.subsA.altaXogo(xogo_X1);
		testUtils.subsB.altaXogo(xogo_X1);
		
		usuarioService.modificacionUsuarioBD(testUtils.subsA);
		usuarioService.modificacionUsuarioBD(testUtils.subsB);

				// Comprobación de recuperación de xogos e subscritores  
				// Agora, subsA subscribiuse a x1 e x2. subsB subscribiuse a x1
				
				listaSubs = xogoService.recuperarSubscritoresXogoBD(xogo_X1);
				assertEquals (2, listaSubs.size());
				assertTrue (listaSubs.contains(testUtils.subsA));
				assertTrue (listaSubs.contains(testUtils.subsB));
		
				listaSubs = xogoService.recuperarSubscritoresXogoBD(xogo_X2);
				assertEquals (1, listaSubs.size());
				assertTrue (listaSubs.contains(testUtils.subsA));
				
				listaXogos = xogoService.recuperarXogosSubscritorBD(testUtils.subsA); 
				assertEquals (2, listaXogos.size());
				assertEquals (xogo_X2, listaXogos.get(0));
				assertEquals (xogo_X1, listaXogos.get(1));
				
				listaXogos = xogoService.recuperarXogosSubscritorBD(testUtils.subsB); 
				assertEquals (1, listaXogos.size());
				assertEquals (xogo_X1, listaXogos.get(0));

				// Comprobación de recuperación de xogos sen subscritores (o mais recente ao FINAL)
				// Agora xa non están nin x1 nin x2
				 
				listaXogos = xogoService.recuperarXogosSenSubscritoresBD();
				assertEquals(3, listaXogos.size());
				assertFalse (listaXogos.contains(xogo_X1));
				assertFalse (listaXogos.contains(xogo_X2));

				// Comprobación de recuperación de xogos sen subscritores dun programador (a mais recente ao FINAL)
				// Na listaxe de progA xa non están x1 nin x2
				
				listaXogos = xogoService.recuperarXogosSenSubscritoresProgramadorBD(progX);
				assertEquals(2, listaXogos.size());
				assertFalse (listaXogos.contains(xogo_X1));
				assertFalse (listaXogos.contains(xogo_X2));
		
				
			
		// Quitámoslle un xogo a un subscritor
		// subsB xa non consta como subscritor de xogo_X1
		
		testUtils.subsB.baixaXogo(xogo_X1);
		usuarioService.modificacionUsuarioBD(testUtils.subsB);

				// Comprobación de recuperación de xogos e subscritores 
				// Agora, subsA subscrito a x1 e x2.  subsB non subscrito a nada. 
				
				listaSubs = xogoService.recuperarSubscritoresXogoBD(xogo_X1);
				assertEquals (1, listaSubs.size());
				assertTrue (listaSubs.contains(testUtils.subsA));
		
				listaSubs = xogoService.recuperarSubscritoresXogoBD(xogo_X2);
				assertEquals (1, listaSubs.size());
				assertTrue (listaSubs.contains(testUtils.subsA));
				
				listaXogos = xogoService.recuperarXogosSubscritorBD(testUtils.subsA); 
				assertEquals (2, listaXogos.size());
				assertEquals (xogo_X2, listaXogos.get(0));
				assertEquals (xogo_X1, listaXogos.get(1));
				
				listaXogos = xogoService.recuperarXogosSubscritorBD(testUtils.subsB); 
				assertEquals (0, listaXogos.size());
	
				// Comprobación de recuperación de xogos sen subscritores (a mais recente ao FINAL)
				
				listaXogos = xogoService.recuperarXogosSenSubscritoresBD();
				assertEquals(3, listaXogos.size());
				assertFalse (listaXogos.contains(xogo_X1));
				assertFalse (listaXogos.contains(xogo_X2));
				
				// Comprobación de recuperación de xogos sen subscritores dun programador (a mais recente ao FINAL)
				
				listaXogos = xogoService.recuperarXogosSenSubscritoresProgramadorBD(progX);
				assertEquals(2, listaXogos.size());
				assertFalse (listaXogos.contains(xogo_X1));
				assertFalse (listaXogos.contains(xogo_X2));
				
		// Probar actualizacions de datos incorrectos
								
		// a) Codigo nulo
				
		String codigo = xogo_X1.getCodigo();
		xogo_X1.setCodigo(null);		
		error = false;
			try {xogoService.modificacionXogoBD(xogo_X1);}
			catch (DataIntegrityViolationException e){error=true;}
		assertTrue(error);
		xogo_X1.setCodigo(codigo);
		
		// b) Data alta nula
				
		Timestamp data = xogo_X1.getDataAlta();
		xogo_X1.setDataAlta(null);		
		error = false;
			try {xogoService.modificacionXogoBD(xogo_X1);}
			catch (DataIntegrityViolationException e){error=true;}
		assertTrue(error);
		xogo_X1.setDataAlta(data);
		
		// c) Programador nulo
				
		Programador progg = xogo_X1.getProgramador();
		xogo_X1.setProgramador(null);		
		error = false;
			try {xogoService.modificacionXogoBD(xogo_X1);}
			catch (DataIntegrityViolationException e){error=true;}
		assertTrue(error);
		xogo_X1.setProgramador(progg);

	}


	
	private void c_Test_Borrado() {
		
		Programador progX;
		Subscritor subsX;
		Xogo xogo_X1, xogo_X2;
		List<Xogo> listaXogos;
		Boolean error;

		// Inicialización
		
		progX = (Programador) usuarioService.recuperarUsuarioBDPorLogin(testUtils.progA.getLogin());
		xogoService.recuperarXogosProgramadorBD(progX);
		assertEquals(4, progX.getXogos().size());
		xogo_X2 = progX.getXogos().get(0);
		xogo_X1 = progX.getXogos().get(1);


		// Tratamos de eliminar xogo con subscritores asociados: non se pode.
		// Hai que eliminar os subscritores antes de eliminar un xogo
		// Actualmente: subsA subscrito a x1 e x2
		
		error = false;
		try{
			xogoService.borradoXogoBD(xogo_X1);   
			progX.baixaXogo(xogo_X1);
		} catch (DataIntegrityViolationException e){error=true;}
		assertTrue(error);
		xogoService.recuperarXogosProgramadorBD(progX);
		assertEquals(4, progX.getXogos().size());
		
		
		// Tratamos de eliminar programador con xogos que ainda teñen subscritores: non se pode
		// progA ten a x1 e x2 con subsA de subscritor

		error = false;
		try{
			usuarioService.borradoUsuarioBD(progX);    
		} catch (DataIntegrityViolationException e){error=true;}
		assertTrue(error);
		progX = (Programador) usuarioService.recuperarUsuarioBDPorLogin(testUtils.progA.getLogin());
		assertNotNull(progX);

		
		// Borramos un subscritor con xogos (pódese)
		
		subsX  = new Subscritor ("SUBSPARABORRAR", "SubsParaBorrar", "SubscritorParaBorrar", Timestamp.valueOf("2017-01-01 10:00:00.0"), "NOVATO",  new Float(15));
		subsX.altaXogo(xogo_X1);
		usuarioService.altaNovoUsuarioBD(subsX);

		subsX = (Subscritor) usuarioService.recuperarUsuarioBDPorId(subsX.getIdUsuario());
		xogoService.recuperarXogosSubscritorBD(subsX);
		assertTrue (subsX.getSubscricions().contains(xogo_X1));
		assertTrue (xogoService.recuperarSubscritoresXogoBD(xogo_X1).contains(subsX));

		usuarioService.borradoUsuarioBD(subsX);
		
		assertNull (usuarioService.recuperarUsuarioBDPorId(subsX.getIdUsuario()));
		assertFalse (xogoService.recuperarSubscritoresXogoBD(xogo_X1).contains(subsX));

		
		// Borramos un xogo sen subscricións
		// Quitámoslle o (único) subscritor a X2, e o borramos 
		
		listaXogos = xogoService.recuperarXogosSenSubscritoresBD();
		assertFalse (listaXogos.contains(xogo_X2));

		subsX = xogoService.recuperarSubscritoresXogoBD(xogo_X2).get(0);
		xogoService.recuperarXogosSubscritorBD(subsX);
		subsX.baixaXogo(xogo_X2);
		usuarioService.modificacionUsuarioBD(subsX);
		
		listaXogos = xogoService.recuperarXogosSenSubscritoresBD();
		assertTrue (listaXogos.contains(xogo_X2));
		
		xogoService.borradoXogoBD(xogo_X2);
		
		xogoService.recuperarXogosProgramadorBD(progX);
		assertEquals(3, progX.getXogos().size());
		xogo_X1 = progX.getXogos().get(0);

		// O mesmo para X1
			
		listaXogos = xogoService.recuperarXogosSenSubscritoresBD();
		assertFalse (listaXogos.contains(xogo_X1));

		subsX.baixaXogo(xogo_X1);
		usuarioService.modificacionUsuarioBD(subsX);

		listaXogos = xogoService.recuperarXogosSenSubscritoresBD();
		assertTrue (listaXogos.contains(xogo_X1));
		
		xogoService.borradoXogoBD(xogo_X1);

		xogoService.recuperarXogosProgramadorBD(progX);
		assertEquals(2, progX.getXogos().size());

	}
}
